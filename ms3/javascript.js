let date = new Date();
let hour = date.getHours();
let minute = date.getMinutes();

if (hour < 10) {
  hour = '0' + hour;
}
if (minute < 10) {
  minute = '0' + minute;
}

let footer = document.querySelector('footer');
let nome = document.querySelector('.nome');
footer.innerHTML = 'Autor: ' + nome.textContent + '<span class="hora">' + hour + ':' + minute + '</span>';

